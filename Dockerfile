FROM registry.gitlab.com/gudenaf/tools/android:latest

WORKDIR /

RUN apt update -y && apt upgrade -y
RUN apt install -y pkg-config clang cmake ninja-build libgtk-3-dev git chromium-browser 
RUN apt autoremove -y

ENV FLUTTER_SDK_ROOT /flutter
ENV PATH $PATH:${FLUTTER_SDK_ROOT}/bin
ENV CHROME_EXECUTABLE /snap/bin/chromium

RUN git clone https://github.com/flutter/flutter.git && ln -s /flutter/bin/flutter /usr/bin/flutter
RUN flutter channel stable
RUN flutter doctor --android-licenses
RUN flutter doctor
